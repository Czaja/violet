package com.horstmann.violet.product.diagram.state;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import com.horstmann.violet.product.diagram.abstracts.Direction;
import com.horstmann.violet.product.diagram.abstracts.edge.IEdge;
import com.horstmann.violet.product.diagram.abstracts.node.RectangularNode;
import com.horstmann.violet.product.diagram.abstracts.property.MultiLineString;
import com.horstmann.violet.workspace.sidebar.colortools.ColorToolsBarPanel;

/**
 * Blok decyzyjny w diagramie stanow
 */
public class DecisionNode extends RectangularNode
{
	/**
	 * Ustawia kolory dla obiektu graficznego
	 */
    public DecisionNode()
    {
        setBackgroundColor(ColorToolsBarPanel.DEFAULT_COLOR.getBackgroundColor());
        setBorderColor(ColorToolsBarPanel.DEFAULT_COLOR.getBorderColor());
        setTextColor(ColorToolsBarPanel.DEFAULT_COLOR.getTextColor());
        name = new MultiLineString();
    }
    
    /** 
     * @return zwraca wszystkie punkty polaczeniowe
     */
    @Override
    public Point2D getConnectionPoint(IEdge e)
    {
        Rectangle2D b = getBounds();

        double x = b.getCenterX();
        double y = b.getCenterY();

        Direction d = e.getDirection(this);

        Direction nearestCardinalDirection = d.getNearestCardinalDirection();
        if (Direction.NORTH.equals(nearestCardinalDirection))
        {
            x = b.getMaxX() - (b.getWidth() / 2);
            y = b.getMaxY();
        }
        if (Direction.SOUTH.equals(nearestCardinalDirection))
        {
            x = b.getMaxX() - (b.getWidth() / 2);
            y = b.getMinY();
        }
        if (Direction.EAST.equals(nearestCardinalDirection))
        {
            x = b.getMinX();
            y = b.getMaxY() - (b.getHeight() / 2);
        }
        if (Direction.WEST.equals(nearestCardinalDirection))
        {
            x = b.getMaxX();
            y = b.getMaxY() - (b.getHeight() / 2);
        }
        return new Point2D.Double(x, y);
    }
    
	/**
	 * Tworzy obszar dla obiektu graficznego
	 */
    @Override
    public Rectangle2D getBounds()
    {
        Rectangle2D b = name.getBounds();
        Rectangle2D textRect = new Rectangle2D.Double(0, 0, Math.max(DEFAULT_WIDTH, b.getWidth()), Math.max(DEFAULT_HEIGHT,
                b.getHeight()));
        double w1 = textRect.getWidth() / 2;
        double h1 = textRect.getHeight() / 2;
        double w2 = Math.tan(Math.toRadians(60)) * h1;
        double h2 = Math.tan(Math.toRadians(30)) * w1;
        Point2D currentLocation = getLocation();
        double x = currentLocation.getX();
        double y = currentLocation.getY();
        double w = (w1 + w2) * 2;
        double h = (h1 + h2) * 2;
        Rectangle2D globalBounds = new Rectangle2D.Double(x, y, w, h);
        Rectangle2D snappedBounds = getGraph().getGridSticker().snap(globalBounds);
        return snappedBounds;
    }
    
    /**
     * Rysuje obiekt
     */
    @Override
    public void draw(Graphics2D g2)
    {
        super.draw(g2);
        Color oldColor = g2.getColor();

        Shape shape = getShape();
        g2.setColor(getBackgroundColor());
        g2.fill(shape);
        g2.setColor(getBorderColor());
        g2.draw(shape);
        
        Rectangle2D shapeRect = getBounds();
        Rectangle2D textRect = name.getBounds();
        textRect.setRect(shapeRect.getCenterX() - textRect.getWidth() / 2, shapeRect.getCenterY() - textRect.getHeight() / 2,
                textRect.getWidth(), textRect.getHeight());
        g2.setColor(getTextColor());
        name.draw(g2, textRect);

        g2.setColor(oldColor);
    }
    
    /**
     * @return Zwraca kszatlt obiektu 
     */
    @Override
    public Shape getShape()
    {
        Rectangle2D shapeRect = getBounds();
        GeneralPath diamond = new GeneralPath();
        float x1 = (float) shapeRect.getX();
        float y1 = (float) shapeRect.getCenterY();
        float x2 = (float) shapeRect.getCenterX();
        float y2 = (float) shapeRect.getY();
        float x3 = (float) (shapeRect.getX() + shapeRect.getWidth());
        float y3 = (float) shapeRect.getCenterY();
        float x4 = (float) shapeRect.getCenterX();
        float y4 = (float) (shapeRect.getY() + shapeRect.getHeight());
        diamond.moveTo(x1, y1);
        diamond.lineTo(x2, y2);
        diamond.lineTo(x3, y3);
        diamond.lineTo(x4, y4);
        diamond.lineTo(x1, y1);
        return diamond;
    }

    /**
     * Zmienia nazwe obiektu 
     * @param newValue Nowa nazwa obiektu
     */
    public void setName(MultiLineString newValue)
    {
        name = newValue;
    }

    /**
     * @return zwraca nazwe obiektu
     */
    public MultiLineString getName()
    {
        return name;
    }

    /**
     * Klonuje obiekt
     * @return zwraca sklonowany obiekt
     */
    @Override
    public DecisionNode clone()
    {
        DecisionNode cloned = (DecisionNode) super.clone();
        cloned.name = (MultiLineString) name.clone();
        return cloned;
    }

    private MultiLineString name;

    private final static int DEFAULT_WIDTH = 30;
    private final static int DEFAULT_HEIGHT = 20;
}