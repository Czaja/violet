package com.horstmann.violet.product.diagram.state;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import com.horstmann.violet.product.diagram.abstracts.edge.IEdge;
import com.horstmann.violet.product.diagram.abstracts.node.INode;
import com.horstmann.violet.product.diagram.abstracts.node.RectangularNode;

/**
 * Synchronizacja w diagramie stanow
 */
public class SynchronizationBarNode extends RectangularNode
{	
    /** 
     * @return zwraca wszystkie punkty polaczeniowe
     */
    @Override
    public Point2D getConnectionPoint(IEdge e)
    {
        Point2D defaultConnectionPoint = super.getConnectionPoint(e);
        
        INode end = e.getEnd();
        INode start = e.getStart();
        if (this == start)
        {
            Point2D endConnectionPoint = end.getConnectionPoint(e);
            double y = defaultConnectionPoint.getY();
            double x = endConnectionPoint.getX();
            return new Point2D.Double(x, y);
        }
        if (this == end)
        {
            Point2D startConnectionPoint = start.getConnectionPoint(e);
            double y = defaultConnectionPoint.getY();
            double x = startConnectionPoint.getX();
            return new Point2D.Double(x, y);
        }

        return defaultConnectionPoint;
    }
    

    /**
     * Ustawia nowa szerokosc i wysokosc po przylaczeniu obiektu 
     * @return nowa granice wysokosci i szerokosci
     */
    @Override
    public Rectangle2D getBounds()
    {
        Rectangle2D b = getDefaultBounds();
        List<INode> connectedNodes = getConnectedNodes();
        if (connectedNodes.size() > 0)
        {
            double minX = Double.MAX_VALUE;
            double maxX = Double.MIN_VALUE;
            for (INode n : connectedNodes)
            {
                Rectangle2D b2 = n.getBounds();
                minX = Math.min(minX, b2.getMinX());
                maxX = Math.max(maxX, b2.getMaxX());
            }

            minX -= EXTRA_WIDTH;
            maxX += EXTRA_WIDTH;

            translate(minX - b.getX(), 0);
            b = new Rectangle2D.Double(minX, b.getY(), maxX - minX, DEFAULT_HEIGHT);
        }
        return b;
    }

    /**
     * Ustawia domyslna szerokosc i wysokosc
     * @return domyslna granice wysokosci i szerokosci
     */
    private Rectangle2D getDefaultBounds()
    {
        Point2D currentLocation = getLocation();
        double x = currentLocation.getX();
        double y = currentLocation.getY();
        double w = DEFAULT_WIDTH;
        double h = DEFAULT_HEIGHT;
        Rectangle2D currentBounds = new Rectangle2D.Double(x, y, w, h);
        return currentBounds;
    }

    /**
     * @return zwraca wszystkie wezly
     */
    private List<INode> getConnectedNodes()
    {
        List<INode> connectedNodes = new ArrayList<INode>();
        // needs to contain all incoming and outgoing edges
        for (IEdge e : getGraph().getAllEdges())
        {
            if (e.getStart() == this) connectedNodes.add(e.getEnd());
            if (e.getEnd() == this) connectedNodes.add(e.getStart());
        }
        return connectedNodes;
    }

    /**
     * Rysuje obiekt
     */
    @Override
    public void draw(Graphics2D g2)
    {
        super.draw(g2);

        Color oldColor = g2.getColor();

        g2.setColor(getBorderColor());
        g2.fill(getShape());

        g2.setColor(oldColor);
    }

    /**
     * Klonuje obiekt
     * @return zwraca z klonowany obiekt
     */
    @Override
    public SynchronizationBarNode clone()
    {
        return (SynchronizationBarNode) super.clone();
    }

    private final static int DEFAULT_WIDTH = 100;
    private final static int DEFAULT_HEIGHT = 5;
    private final static int EXTRA_WIDTH = 12;
}
