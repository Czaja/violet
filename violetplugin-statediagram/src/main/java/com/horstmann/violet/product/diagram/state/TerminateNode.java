package com.horstmann.violet.product.diagram.state;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import com.horstmann.violet.product.diagram.abstracts.node.RectangularNode;
import com.horstmann.violet.product.diagram.abstracts.property.MultiLineString;
import com.horstmann.violet.workspace.sidebar.colortools.ColorToolsBarPanel;

/**
 * Terminate w diagramie stanow
 */
public class TerminateNode extends RectangularNode
{
	/**
	 * Ustawia kolory dla obiektu graficznego
	 */
   public TerminateNode()
   {
	   super();
       setBackgroundColor(ColorToolsBarPanel.DEFAULT_COLOR.getBackgroundColor());
       setBorderColor(ColorToolsBarPanel.DEFAULT_COLOR.getBorderColor());
       setTextColor(ColorToolsBarPanel.DEFAULT_COLOR.getTextColor());
       name = new MultiLineString();
   }
   
   /**
	* Tworzy obszar dla obiektu graficznego
	*/
   @Override
   public Rectangle2D getBounds()
   {
       Rectangle2D b = name.getBounds();
       Point2D currentLocation = getLocation();
       double x = currentLocation.getX();
       double y = currentLocation.getY();
       double w = Math.max(b.getWidth(), DEFAULT_DIAMETER);
       double h = Math.max(b.getHeight(), DEFAULT_DIAMETER);
       Rectangle2D currentBounds = new Rectangle2D.Double(x, y, w, h);
       Rectangle2D snappedBounds = getGraph().getGridSticker().snap(currentBounds);
       return snappedBounds;
   }

   /**
    * Rysuje 2 linie pod katem prostym
    */
   @Override
   public void draw(Graphics2D g2)
   {
       Color oldColor = g2.getColor();

       Rectangle2D bounds = getBounds();

       GeneralPath path = new GeneralPath();
       
       path.moveTo(bounds.getX() + DEFAULT_DIAMETER / DEFAULT_HEIGHT_X, bounds.getY() + DEFAULT_DIAMETER / DEFAULT_HEIGHT_X);
       path.lineTo(bounds.getX() + DEFAULT_DIAMETER / DEFAULT_WIDTH_X, bounds.getY() + DEFAULT_DIAMETER / DEFAULT_WIDTH_X);
       path.moveTo(bounds.getX() + DEFAULT_DIAMETER / DEFAULT_WIDTH_X, bounds.getY() + DEFAULT_DIAMETER / DEFAULT_HEIGHT_X);
       path.lineTo(bounds.getX() + DEFAULT_DIAMETER / DEFAULT_HEIGHT_X, bounds.getY() + DEFAULT_DIAMETER / DEFAULT_WIDTH_X);
    
       g2.setColor(getBorderColor());
       g2.draw(path);
       g2.setColor(oldColor);
   }

   /**
    * Klonuje obiekt
    * @return zwraca sklonowany obiekt
    */
   @Override
   public TerminateNode clone()
   {
	   TerminateNode cloned = (TerminateNode) super.clone();
       cloned.name = (MultiLineString) name.clone();
       return cloned;
   }
   
   private MultiLineString name;

   private static int DEFAULT_DIAMETER = 20;
   private final static double DEFAULT_HEIGHT_X = 6.66;
   private final static double DEFAULT_WIDTH_X = 1.17;
}
