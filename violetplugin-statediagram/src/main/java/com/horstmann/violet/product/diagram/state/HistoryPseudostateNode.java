package com.horstmann.violet.product.diagram.state;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import com.horstmann.violet.product.diagram.abstracts.node.EllipticalNode;
import com.horstmann.violet.product.diagram.abstracts.property.MultiLineString;
import com.horstmann.violet.workspace.sidebar.colortools.ColorToolsBarPanel;

/**
* An initial or final node (bull's eye) in a state or activity diagram.
*/
public class HistoryPseudostateNode extends EllipticalNode
{
	public HistoryPseudostateNode()
    {
        super();
        setBackgroundColor(ColorToolsBarPanel.DEFAULT_COLOR.getBackgroundColor());
        setBorderColor(ColorToolsBarPanel.DEFAULT_COLOR.getBorderColor());
        setTextColor(ColorToolsBarPanel.DEFAULT_COLOR.getTextColor());
        name = new MultiLineString();
    }

    @Override
    public Rectangle2D getBounds()
    {
    	Rectangle2D b = name.getBounds();
        Point2D currentLocation = getLocation();
        double x = currentLocation.getX();
        double y = currentLocation.getY();
        double w = Math.max(b.getWidth(), DEFAULT_DIAMETER);
        double h = Math.max(b.getHeight(), DEFAULT_DIAMETER);
        Rectangle2D currentBounds = new Rectangle2D.Double(x, y, w, h);
        Rectangle2D snappedBounds = getGraph().getGridSticker().snap(currentBounds);
        return snappedBounds;
    }


    public void draw(Graphics2D g2)
    {
        super.draw(g2);

        Color oldColor = g2.getColor();

        Ellipse2D circle = new Ellipse2D.Double(getBounds().getX(), getBounds().getY(), getBounds().getWidth(), getBounds()
                .getHeight());

        g2.setColor(getBackgroundColor());
        g2.fill(circle);
        g2.setColor(getTextColor());
        name.draw(g2, getBounds());     
        g2.setColor(getBorderColor());
        g2.draw(circle);

        g2.setColor(oldColor);
    }
    
    public void setName(MultiLineString newValue)
    {
        name = newValue;
    }
    
    public MultiLineString getName()
    {
        return name;
    }
    
    private MultiLineString name;
    private final static int DEFAULT_DIAMETER = 25;
}